/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';

import LoadingPage from './src/page/LoadingPage';
import LoginPage from './src/page/LoginPage';
import HomePage from './src/page/HomePage';
import RegisterPage from './src/page/RegisterPage';

import Router from './src/system/Router';

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => LoadingPage);
