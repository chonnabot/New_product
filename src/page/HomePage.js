import React from 'react'
import { View, Text, Alert } from 'react-native';
import HandleBack from '../../back'
import Header from '../styled_components/header'
import {
    Background,
    SubmitButton,
    BodyContainer
} from '../styled_components/styled_components'


class HomePage extends React.Component {
    // state = {
    //     editing: false,
    // }

    // onBack = () => {
    //     if(this.state.editing){
    //         Alert.alert(
    //             "You're still editing",
    //             "Are you sure you want to go home with your edits not saved?",
    //             [
    //                 {text: "Keep Editing", onPress: () => {}, style: "cancel"},
    //                 {text: "Go Home", onPress: () => this.props.navigation.goBack()},
                    
    //             ],
    //             { cancelable : false},
    //         )
    //         return true
    //     }
    //     return false
    // }
    render() {
        return (
            // <HandleBack onBack={this.onBack}>
                <Background>

                    <Header />

                    <BodyContainer>
                        <SubmitButton type="primary" >Submit</SubmitButton>

                    </BodyContainer>
                </Background>


            // </HandleBack>
        )
    }
}

export default HomePage