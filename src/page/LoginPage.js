import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Icon, WhiteSpace, InputItem } from '@ant-design/react-native'

import {
    Container,
    LoginText,
    UserInput,
    InputBox,
    IconBox,
    Image,
    SubmitButton,
    NextText,
    HeaderText
} from '../styled_components/styled_components'

import Imgebackground from '../image/background.jpg'
import Cat from '../image/cat.gif'

class LoginPage extends React.Component {
    state = {
        email: 'tamlks@hotmail.com',
        password: '23400949',
        isLoading: false
    }

    goToHomepage = () => {
        return this.props.history.push('/HomePage')
    }

    goToRegisterPage = () => {
        return this.props.history.push('/RegisterPage')
    }


    render() {
        return (
            <Container source={Imgebackground} >

                <LoginText>เข้าสู่ระบบ</LoginText>
                <Image source={Cat} />

                <WhiteSpace />
                <WhiteSpace />
                <InputBox>
                    <IconBox name='user'
                        size={26}
                    />
                    <UserInput
                        clear
                        placeholder={'อีเมลล์'}
                        placeholderTextColor={'rgba(0,0,0,0.35)'}
                        value={this.state.email}
                        onChange={value => this.setState({ email: value })}
                    />
                </InputBox>

                <WhiteSpace />
                <InputBox>
                    <IconBox name='lock'
                        size={26}
                    />
                    <UserInput
                        clear
                        type='password'
                        placeholder={'รหัสผ่าน'}
                        placeholderTextColor={'rgba(0,0,0,0.35)'}
                        value={this.state.password}
                        onChange={value => this.setState({ password: value })}
                    />
                </InputBox>
                <WhiteSpace />
                <WhiteSpace />

                <SubmitButton type="primary" onPress={this.goToHomepage} >ยืนยัน</SubmitButton>
                <WhiteSpace />
                <WhiteSpace />
                {/* <Line></Line> */}
                <TouchableOpacity onPress={this.goToRegisterPage}>
                    <NextText >สร้างบัญชีผู้ใช้ใหม่</NextText>
                </TouchableOpacity>

            </Container >
        );
    }
}


export default LoginPage



