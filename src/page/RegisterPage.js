import React from 'react';
import { View, Text, TouchableOpacity,  } from 'react-native';
import { Icon, WhiteSpace, InputItem } from '@ant-design/react-native'

import {
    Container,
    LoginText,
    UserInput,
    InputBox,
    IconBox,
    Image,
    SubmitButton,
    Line,
    NextText
} from '../styled_components/styled_components'

import Imgebackground from '../image/background.jpg'
import Cat from '../image/cat.gif'

class LoginPage extends React.Component {
    state = {
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        isLoading: false
    }

    goToLoginpage = () => {
        return this.props.history.push('/Loginpage')
    }


    render() {
        return (
            <Container>

                <LoginText>ลงทะเบียน</LoginText>
                <WhiteSpace />
                <WhiteSpace />
                <WhiteSpace />

                <InputBox>
                    <IconBox name='mail'
                        size={26}
                    />
                    <UserInput
                        clear
                        placeholder={'อีเมลล์'}
                        placeholderTextColor={'rgba(0,0,0,0.35)'}
                        value={this.state.email}
                        onChange={value => this.setState({ email: value })}
                    />
                </InputBox>
                <WhiteSpace />

                <InputBox>
                    <IconBox name='lock'
                        size={26}
                    />
                    <UserInput
                        clear
                        type='password'
                        placeholder={'รหัสผ่าน'}
                        placeholderTextColor={'rgba(0,0,0,0.35)'}
                        value={this.state.password}
                        onChange={value => this.setState({ password: value })}
                    />
                </InputBox>
                <WhiteSpace />

                <InputBox>
                    <IconBox name='solution'
                        size={26}
                    />
                    <UserInput
                        clear
                        placeholder={'ชื่อ'}
                        placeholderTextColor={'rgba(0,0,0,0.35)'}
                        value={this.state.firstname}
                        onChange={value => this.setState({ firstname: value })}
                    />
                </InputBox>
                <WhiteSpace />

                <InputBox>
                    <IconBox name='solution'
                        size={26}
                    />
                    <UserInput
                        clear
                        placeholder={'นามสกุล'}
                        placeholderTextColor={'rgba(0,0,0,0.35)'}
                        value={this.state.lastname}
                        onChange={value => this.setState({ lastname: value })}
                    />
                </InputBox>
                <WhiteSpace />
                <WhiteSpace />

                <SubmitButton type="primary" onPress={this.goToLoginpage} >ยืนยัน</SubmitButton>
                <WhiteSpace />
                <WhiteSpace />

                {/* <Line></Line> */}
                <TouchableOpacity onPress={this.goToLoginpage}>
                    <NextText >
                        <IconBox name='arrow-left'size={16}/> กลับ
                    </NextText>
                </TouchableOpacity>

            </Container >
        );
    }
}


export default LoginPage



