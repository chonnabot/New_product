import React from 'react'

import { HeaderContainer, HeaderText } from './styled_components'

class header extends React.Component{
    render(){
        return(
            <HeaderContainer>
                <HeaderText>
                    Home
                </HeaderText>

            </HeaderContainer>
        )
    }
}
export default header
