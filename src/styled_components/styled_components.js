import styled from 'styled-components'
import { Button, InputItem, Icon, Modal } from '@ant-design/react-native'


export const Container = styled.View`
    align-items: center;
    background-color: #FCF0E4;
    padding-top: 35px;
    flex : 1;
`

export const ContainerCenert = styled.View`
    align-items: center;
    justify-content: center;
    background-color: #FCF0E4;
    flex : 1;
`
export const Background = styled.View`
    justify-content: center;
    backgroundColor: #003;
    flex : 1;
`
export const Image = styled.Image`
    background-color: white;
    align-items: center;
    justify-content: center;
    width: 200px;
    height: 200px;
   
`
export const BodyContainer = styled.View`
    background-color: #778899;
    align-items: center;
    justify-content: center;
    flex : 1;
`
export const HeaderContainer = styled.View`
    flex-direction: row;
    align-items: center;
    justify-content: center;
    background-color: #f2f2f2
    height: 50
    padding-top: 5px;
    padding-bottom: 5px;
`
export const LoginText = styled.Text`
    color : black;
    font-family: 'Mali-Bold';
    font-size: 36px;
`
export const LoadText1 = styled.Text`
    color : #63544C;
    font-family: 'Mali-Bold';
    font-size: 38px;
`
export const LoadText2 = styled.Text`
    color : #B4A89C;
    font-family: 'Mali-Bold';
    font-size: 30px;
    top: -30px;
`
export const NextText = styled.Text`
    font-family: Mali-Medium;
   
`
export const HeaderText = styled.Text`
    font-family: Mali-Medium;
    font-size: 20px;
   
`
export const InputBox = styled.View`
    width: 280px;   
`

// border-width: 1; 
// border-color: #d7dae0; 
// border-radius: 6; 
// padding-horizontal: 14; 

export const Line = styled.View`
    width: 150px;
    height: 0.5px;
    background-color: #DCDCDC;    
`  

export const IconBox = styled(Icon)`
    top: 5px;
    color: rgba(0,0,0,0.5);
    position: absolute;
`
export const UserInput = styled(InputItem)`
    color: black; 
    padding-left: 20px;
    font-family: Mali-Medium;
    height: 150%;
`

export const SubmitButton = styled(Button)`
    width: 30%;
    border-radius: 20px;
`
export const ModalRegister = styled(Modal)`s
  
`