import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { store, history } from './AppStore'


import LoginPage from '../page/LoginPage'
import HomePage from '../page/HomePage';
import RegisterPage from '../page/RegisterPage';





class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/LoginPage" component={LoginPage} />
                        <Route exact path="/HomePage" component={HomePage} />
                        <Route exact path="/RegisterPage" component={RegisterPage} />
                        <Redirect to="/LoginPage" />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}



export default Router
